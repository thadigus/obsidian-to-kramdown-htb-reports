# Obsidian to Kramdown HTB Reports

A simple bash script to convert my Obsidian Markdown written HackTheBox and TryHackMe reports to a Kramdown compatible Markdown blog posts for my blog: https://thadigus.gitlab.io 

I don't recommend that anyone uses this. I wanted to post this script as a small yet terrible example of my Bash scripting skills.
