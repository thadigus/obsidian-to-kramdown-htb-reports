#!/bin/bash

# Help banner and parameter parsing
echo 'Args:
$1 - Path to Markdown file of article to convert ex: ./Clock.md
$2 - Desired article name ex: 2022-02-08-Conceal-HTB-Writeup
$3 - Machine/Lab name
$4 - Platform name ex: HTB'

if (( $# != 4 )); then
	>&2 echo "Not enough parameters"
	exit
fi

echo "Path to Markdown file: $1"
echo "Desired article name: $2"
echo "Machine/Lab name: $3"
echo "Platform name: $4"

# Create environment to work
echo "Creating working environment folders."
mkdir "$2"

# Add article metadata
echo "Adding article metadata."
echo "---
title: \"$3 - $4 Writeup\"
---" > "$2.md"

# Add whitespace between elements
echo "Doubling up whitespace."
sed '/^$/d;G' "$1" >> "$2.md"

# Find out how many rows are in table
older_header=$(cat "$2.md" | grep '|* -' | tail -1)
if [ -z "$older_header" ]
then
	echo "Fixing tables"
	cols=$(cat "$2.md" | grep '|* -' | tail -1 | grep -o '|:' | wc -l)
	echo "Header found: $older_header"
	echo "Number of columns in table: $cols"

	# Add header to top of table with the correct columns ex: |---+---+---+---|
	table_header='|---'
	until [ $(($cols)) -eq 1 ]; do
		table_header+='+---'
		cols="$(($cols - 1))"
	done
	table_header+='|'
	top_line=$(cat "$2.md" | grep '|' | head -1)
	new_top="$table_header""\n\n""$top_line"
	echo "Adding header: $table_header."
	sed -i -e "s/$top_line/$new_top/g" "$2.md"

	# Remove double spacing in tables, this will only append new table to the bottom of the file
	echo "Single spacing table."
	old_spaced_table=$(cat "$2.md" | grep "|*|" | sed '/^$/d;G')
	single_spaced_table=$(cat "$2.md" | grep "|*|")
	sed '/|/d' "$2.md" > "temp.md"
	mv temp.md "$2.md"
	sed '/^$/d' "$2.md" > "temp.md"
	sed '/^$/d;G' "temp.md" > "$2.md"
	rm -rf temp.md
	echo "$single_spaced_table" >> "$2.md"
fi

# Locate and copy screenshots into folder
echo "Locate screenshots."
IFS=$'\n'
screenshots=($(cat "$2.md" | grep Screenshot | awk 'sub(/^.{3}/,"")' | awk '{print substr($0, 1, length($0)-2)}' | sed 's/ /\\ /g'))
for i in "${screenshots[@]}"
do 
	echo $i
	pic_path=$(find / -name "$i" 2>/dev/null | tail -1)
	echo "$pic_path"
	cp "$pic_path" "$2"
done

# Format screenshots
echo "Replacing image files references."
sed -i -e "s|\!\[\[Screenshot|\!\[Screenshot\]\(\/assets\/images\/$2\/Screenshot|g" "$2.md"
sed -i -e "s|\]\]|\)|g" "$2.md"